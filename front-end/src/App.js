import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import UrlShortener from "./containers/UrlShortener";


class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={UrlShortener} />
      </Switch>
    );
  }
}

export default App;
