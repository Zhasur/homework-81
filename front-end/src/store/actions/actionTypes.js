import axios from '../../axios-api';

export const FETCH_URL_SUCCESS = 'FETCH_URL_SUCCESS';

export const fetchLinksSuccess = url => ({type: FETCH_URL_SUCCESS, url});

export const postUrl = (urlData) => {
    return dispatch => {
        return axios.post('/shortUrl', urlData).then(response => {
            dispatch(fetchLinksSuccess(response.data))
        }).catch(error => error)
    }
};