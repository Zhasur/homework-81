import React, {Component} from 'react';
import {connect} from "react-redux";
import {postUrl} from "../store/actions/actionTypes";

import './UrlShortener.css'

class UrlShortener extends Component {
    state = {
        originalUrl: ''
    };

    componentDidMount() {
        this.props.postUrl()
    }

    urlOnChange = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value})
    };

    render() {
        console.log(this.props.url);
        return (
            <div className="main-block">
                <h1>Shorten your link!</h1>
                <input
                    name="originalUrl"
                    onChange={this.urlOnChange}
                    type="text"
                    value={this.state.originalUrl}
                />
                <button
                    onClick={() => this.props.postUrl(this.state)}
                    style={{display: "block", margin: "6px auto"}}
                >
                    Shorten!
                </button>
                <div>
                    <h4>Your link new looks like this: </h4>
                    <a href={"http://localhost:8050/shortUrl/" + this.props.url.shortUrl}>{this.props.url.shortUrl}</a>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        url: state.url.url
    }
};

const mapDispatchToProps = dispatch => {
    return {
        postUrl: (urlData) => dispatch(postUrl(urlData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UrlShortener);


