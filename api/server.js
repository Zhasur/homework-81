const express = require('express');
const cors = require('cors');
const shortUrl = require('./app/shortUrl');
const mongoose = require('mongoose');
const app = express();

app.use(express.json());
app.use(cors());

const port = 8050;

mongoose.connect('mongodb://localhost/shortUrl', {useNewUrlParser: true}).then(() => {
    app.use('/shortUrl', shortUrl);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});

