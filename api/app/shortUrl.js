const express = require('express');
const nanoid = require('nanoid');
const ShortUrl = require('../modals/ShortUrl');
const router = express.Router();


router.get('/:id', (req, res) => {
    ShortUrl.findOne({shortUrl: req.params.id})
        .then(url => {
            if (url) {
                return res.status(301).redirect(url.originalUrl);
            }
            res.sendStatus(404);

        })
        .catch(() => res.sendStatus(500));
});

router.post('/', (req, res) => {
    const urlData = req.body;

    const shortUrl = new ShortUrl(urlData);
    shortUrl.shortUrl = nanoid(7);

    shortUrl.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});
module.exports = router;
