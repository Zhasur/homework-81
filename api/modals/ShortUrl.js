const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UrlSchema = new Schema({
    shortUrl: {
        type: String, required: true
    },
    originalUrl: {
        type: Number, required: true
    }
});

const ShortUrl = mongoose.model('ShortUrl', UrlSchema);
module.exports = ShortUrl;